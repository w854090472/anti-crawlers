from selenium_base import *

url = 'http://47.103.13.124:8001/captcha_slide_puzzle'

try:
    brower = get_brower()
    brower.get(url)
    brower.add_cookie({"name": "session", "value": ".eJyrViotTi1SsqpWyiyOT0zJzcxTsjLQUcrJTwexSopKU3WUcvOTMnNSlayUDM3gQEkHrDE-M0XJyhjCzkvMBSmKKTVNMjMDkiamFkq1tQDfeR3n.YLOC4w.Xbnx1QbrvUh8OUPb5jauC_Aau9U"})
    brower.get(url)  # 再次访问，使用加载的Cookies
    jigsawCircle = brower.find_element_by_id('jigsawCircle')
    action = webdriver.ActionChains(brower)

    action.click_and_hold(jigsawCircle).move_by_offset(1, 0).release().perform()
    wait_element(brower, 'targetblock')
    # 因为targetblock是动态加载的，加载后left属性可能没有被JS运算获得
    # 此时left为auto，为了获得正确的left，这里使用一个简单循环
    for i in range(10):
        targetblock = brower.find_element_by_id('targetblock')
        # 获取目标缺口的left值
        left_value = targetblock.value_of_css_property('left')
        try:
            left_value = float(left_value.replace('px', ''))
            break
        except:
            time.sleep(0.5)
    # 12 怎么来的？观察出来的
    action.click_and_hold(jigsawCircle).move_by_offset(left_value - 12, 0).perform()
    time.sleep(0.5)
    action.release().perform()

finally:
    time.sleep(5)
    brower.close()