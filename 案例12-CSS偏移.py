import re
import requests
from bs4 import BeautifulSoup

cookies ={
    "session": ".eJyrViotTi1SsqpWyiyOT0zJzcxTsjLQUcrJTwexSopKU3WUcvOTMnNSlayUDM3gQEkHrDE-M0XJyhjCzkvMBSmKKTVNMjMDkiamFkq1tQDfeR3n.YLOC4w.Xbnx1QbrvUh8OUPb5jauC_Aau9U"
}

headers = {
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_16_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.92 Safari/537.36"
}

url = "http://47.103.13.124:8001/flight"

r = requests.get(url, cookies=cookies, headers=headers)
soup = BeautifulSoup(r.text, 'lxml')

spans = soup.find_all('span', class_='prc_wp')
for span in spans:
    all_b = span.find_all('b')
    first_b = all_b[0]
    pattern = re.compile('width:(\d+)px')
    style = first_b.attrs.get('style')
    # 第一个b标签的width
    first_b_width = int(pattern.findall(style)[0])
    # 第一个b标签中所有的值
    first_b_values = first_b.get_text().replace('\n', '').replace(' ', '')

    # 其余b标签的left属性与具体的值
    other_d = []
    pattern = re.compile('left:-(\d+)px')
    for d in all_b[1:]:
        style = d.attrs.get('style')
        left = pattern.findall(style)[0]
        other_d.append({
            'left': left,
            'value': d.get_text()
        })

    # 每个元素的大小
    size = first_b_width / len(first_b_values)
    result = [i for i in first_b_values]

    for d in other_d:
        # 其余b标签中left值 处于 size 获得下标
        # 比如left=-72px，72 / 18 = 4
        index = int(int(d['left']) / size)
        # 因为left是负数，所以index需要取负数
        result[-index] = d['value']

    print('result: ', ''.join(result))