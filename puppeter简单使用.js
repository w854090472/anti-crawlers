const puppeteer = require('puppeteer');

async function getPic() {
  // 启动 puppeteer
  const browser = await puppeteer.launch({headless:false});
  const page = await browser.newPage();
  await page.goto('https://www.baidu.com/');
  await page.screenshot({path: 'baidu.png'});

  await browser.close();
}

getPic();